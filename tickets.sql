-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-02-2024 a las 23:18:46
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tickets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client`
--

CREATE TABLE `client` (
  `id_cli` int(11) NOT NULL,
  `name_cli` varchar(50) DEFAULT NULL,
  `lastname_cli` varchar(50) DEFAULT NULL,
  `card_cli` int(11) DEFAULT NULL,
  `direction_cli` varchar(50) DEFAULT NULL,
  `mail_cli` varchar(50) DEFAULT NULL,
  `phone_cli` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `client`
--

INSERT INTO `client` (`id_cli`, `name_cli`, `lastname_cli`, `card_cli`, `direction_cli`, `mail_cli`, `phone_cli`) VALUES
(6, NULL, NULL, NULL, NULL, NULL, NULL),
(7, NULL, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'sdfsdf', NULL, NULL, NULL, NULL, NULL),
(18, 'dayama', 'perez', NULL, 'zZDZDasfasf', 'asfdasdasdasdasda', '0996861363'),
(20, 'lllllllllllllllll', 'rrrrrrrrrrrrrrrrrrrrrrrr', NULL, 'fffffffffffffffffffffffffffff', 'iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii', '0996862365'),
(21, 'Bryan', 'asdad', NULL, 'asda', 'asdad', '1234567789');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stadium`
--

CREATE TABLE `stadium` (
  `name_est` varchar(50) NOT NULL,
  `id_est` int(11) NOT NULL,
  `direction_est` varchar(50) DEFAULT NULL,
  `capacity_est` int(11) DEFAULT NULL,
  `location_est` varchar(50) DEFAULT NULL,
  `doors_ingreso_est` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `stadium`
--

INSERT INTO `stadium` (`name_est`, `id_est`, `direction_est`, `capacity_est`, `location_est`, `doors_ingreso_est`) VALUES
('', 1, 'porti', 2222, 'porti', '8');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `team`
--

CREATE TABLE `team` (
  `name_equi` varchar(50) NOT NULL,
  `id_equi` int(11) NOT NULL,
  `template_equi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `team`
--

INSERT INTO `team` (`name_equi`, `id_equi`, `template_equi`) VALUES
('sdfsd', 2, 'sdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `id_ticket` int(11) NOT NULL,
  `worth_ticket` float DEFAULT NULL,
  `time_ticket` varchar(50) DEFAULT NULL,
  `date_ticket` varchar(50) DEFAULT NULL,
  `seat_ticket` varchar(50) DEFAULT NULL,
  `door_ingreso` varchar(50) DEFAULT NULL,
  `name_equipo` varchar(50) NOT NULL,
  `name_estadio` varchar(50) NOT NULL,
  `card_cli` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`id_ticket`, `worth_ticket`, `time_ticket`, `date_ticket`, `seat_ticket`, `door_ingreso`, `name_equipo`, `name_estadio`, `card_cli`) VALUES
(2, 20, '423', '1213', 'adasd', '4', 'Liga', 'Rodrigo Paz', 0),
(3, 65, '65', '656565', '65', '65', 'tres', 'llllllllllllllll', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id_cli`),
  ADD UNIQUE KEY `cedula_cli` (`card_cli`);

--
-- Indices de la tabla `stadium`
--
ALTER TABLE `stadium`
  ADD PRIMARY KEY (`id_est`),
  ADD UNIQUE KEY `nombre_est` (`name_est`);

--
-- Indices de la tabla `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id_equi`),
  ADD UNIQUE KEY `nombre_equi` (`name_equi`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id_ticket`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `client`
--
ALTER TABLE `client`
  MODIFY `id_cli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `stadium`
--
ALTER TABLE `stadium`
  MODIFY `id_est` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `team`
--
ALTER TABLE `team`
  MODIFY `id_equi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id_ticket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
